Rails.application.routes.draw do

  get "/compliance/engine/readme" => "compliance_engine#index"
  get "/compliance/engine/configure" => "compliance_engine#configure"
  get "/compliance/engine/installer" => "compliance_engine#installer"

  post "/compliance/installer/before_process" => "compliance_installer#before_process"
  post "/compliance/installer/core_process" => "compliance_installer#core_process"
  post "/compliance/installer/post_process" => "compliance_installer#post_process"
  post "/compliance/installer/uninstall" => "compliance_installer#uninstall"

  # Services
  get "/compliance/services/feature/status" => "compliance_services#get_compliance_feature_status"
  get "/compliance/services/feature/versioning/status" => "compliance_services#get_versioning_status"
  post "/compliance/services/feature/status/set" => "compliance_services#set_compliance_feature_status"
  post "/compliance/services/feature/versioning/status/set" => "compliance_services#set_versioning_status"
end
