$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rxcms-compliance_plugin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rxcms-compliance_plugin"
  s.version     = RxcmsCompliancePlugin::VERSION
  s.authors     = ["Anh Nguyen","Julian Cowan"]
  s.email       = ["rxcms@ngonluakiko.com"]
  s.homepage    = "https://bitbucket.org/kikoflame/rxcms-compliance_plugin"
  s.summary     = "Versioning and peer review capability."
  s.description = "Plugin Extension for RXCMS that provides versioning and peer review capability for all content."
  s.licenses    = ["MIT"]
  
  s.metadata = {
      "compatCode" => "2dd89a9ecdeb920d539954fd9fb3da7de9f52a61ee9895d06e4bc1bab11a598b",
      "fullName" => "Compliance Plugin",
      "mountPoint" => "/compliance/engine/configure",
      "installerPoint" => "/compliance/engine/installer"
  }

  s.files = Dir["{app,config,db,lib}/**/*"] + ["LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 3.2.13"
  s.add_dependency "sqlite3"
  s.add_dependency "mysql2"
  s.add_dependency "pg"
  s.add_dependency 'paper_trail', '~> 3.0.0.beta1'
  s.add_dependency 'flag_shih_tzu'
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency 'paper_trail', '~> 3.0.0.beta1'
  s.add_development_dependency 'flag_shih_tzu'
  s.add_development_dependency 'rspec-rails'
end
