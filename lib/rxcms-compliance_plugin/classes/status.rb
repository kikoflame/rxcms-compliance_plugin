module RxcmsCompliancePlugin

  class Status

    def self.isEnabled?(siteid)
      app_config = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../../config/compliance/compliance_config.yml', __FILE__))))

      tHash = Hash.new

      tHash['compliance'] = Metadata.first({:conditions => ['sites_id = ? and key = ?', siteid, app_config[:COMPLIANCE_ACTIVATION_CONFIG]]}).value
      tHash['versioning'] = Metadata.first({:conditions => ['sites_id = ? and key = ?', siteid, app_config[:COMPLIANCE_VERSIONING_ACTIVATION_CONFIG]]}).value

      return tHash
    end

  end

end