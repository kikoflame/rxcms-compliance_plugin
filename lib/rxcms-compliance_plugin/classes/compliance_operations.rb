module RxcmsCompliancePlugin

  class ComplianceOperations
    # Used for enforcing compliance of metadata

    def self.process_content(siteid, obj)
      if (getFeatureStatus(siteid, 'compliance') == "yes")
        if (!obj.nil?)
          if (obj.archived?)
            raise
          end

          release = Release.find_by_metadata_id(obj.id)
          if (release.nil?)
            raise
          else
            # check versions and return the right content
            if (release.date_time.nil?)
            else
              obj = obj.version_at(release.date_time)
            end
          end
        end
      end

      return obj
    end

    def self.process_content_with_flag(siteid, obj)
      cFlag = true
      if (getFeatureStatus(siteid, 'compliance') == "yes")
        if (!obj.nil?)
          if (obj.archived?)
            cFlag = false
          else
            release = Release.find_by_metadata_id(obj.id)
            if (release.nil?)
              cFlag = false
            else
              # check versions and return the right content
              if (release.date_time.nil?)
              else
                obj = obj.version_at(release.date_time)
              end
            end
          end
        else

        end
      end

      return { :data => obj, :flag => cFlag }
    end

    def self.create
      # Do nothing
    end

    def self.update(id, dataObj, current_user_id, affectedCategory, siteid, time)
      status = getFeatureStatus(siteid, 'compliance')
      versioningStatus = getFeatureStatus(siteid, 'versioning')

      if (!status.nil? && !versioningStatus.nil?)

        if (versioningStatus == "yes")
          #---
          Metadata.paper_trail_on
          if (status == "yes")
            if (affectedCategory.include?(Metadata.find(id.to_i).cat))
              Metadata.update(id, dataObj)

              if (time != "current")
                # Allow update changes but doesn't allow publishing
                modifier = Version.select("whodunnit").where("created_at >= ? and created_at <= ?", DateTime.parse(time) - 1.second, DateTime.parse(time) + 1.second).first()

                if (modifier[:whodunnit].to_i != current_user_id)
                  release = Release.first({:conditions => ['metadata_id = ?', id]})
                  if (!release.nil?)
                    release.destroy
                  end
                  Release.create({
                                     :metadata_id => id,
                                     :date_time => DateTime.parse(time),
                                     :users_id => current_user_id
                                 })
                else
                  raise "own-change"
                end
              else
                # raise "the content is current"
                release = Release.first({:conditions => ['metadata_id = ?', id]})
                if (!release.nil?)
                  if (release.users_id != current_user_id)
                    release.destroy
                    Release.create({
                                       :metadata_id => id,
                                       :date_time => DateTime.now,
                                       :users_id => current_user_id
                                   })
                  else
                    raise "own-change"
                  end
                else
                  Release.create({
                                     :metadata_id => id,
                                     :date_time => DateTime.now,
                                     :users_id => current_user_id
                                 })
                end

              end

              return "saved_published"
            else
              # if not in affected category, should not be tracking
              Metadata.update(id, dataObj)
            end
          else
            raise "fallback"
          end
          #---
        else
          # raise "versioning feature status is not found"
          Metadata.paper_trail_off
          Metadata.update(id, dataObj)
          return "saved"
        end

      else
        # raise "compliance feature bundle status is not found"
        Metadata.paper_trail_off
        Metadata.update(id, dataObj)
        return "saved"
      end

    end

    def self.destroy(siteid, affectedCategory, metadata)
      versioningStatus = getFeatureStatus(siteid, 'versioning')

      if (!versioningStatus.nil?)
        if (versioningStatus == "yes")
          if (affectedCategory.include?(metadata.cat))
            metadata.archived = true
            metadata.save
          else
            Metadata.paper_trail_on
            metadata.destroy
          end
        else
          Metadata.paper_trail_off
          metadata.destroy
        end
      else
        Metadata.paper_trail_off
        metadata.destroy
      end

    end

    def self.destroy_permanently(id)
      begin
        # Destroy item in metadata
        metadata = Metadata.first({ :conditions => ['id = ?', id] })
        if (!metadata.nil?)
          Metadata.paper_trail_off
          metadata.destroy
        end

        release = Release.first({ :conditions => ['metadata_id = ?', id]})
        if (!release.nil?)
          release.destroy
        end

        # And, destroy item in versions
        Version.destroy_all(:item_id => id)

      rescue Exception => ex
        raise ex.message
      end
    end

    def self.restore(id)
      begin
        item = Metadata.find(id)
        item.archived = false
        item.save
      rescue Exception => ex
        raise ex.message
      end
    end

    def self.getFeatureStatus(siteid, type)
      app_config = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../../config/compliance/compliance_config.yml', __FILE__))))

      if (type == 'compliance')
        status = Metadata.first({:conditions => ['sites_id = ? and key = ?', siteid, app_config[:COMPLIANCE_ACTIVATION_CONFIG]]})
        if (!status.nil?)
          return status.value
        else
          return nil
        end
      elsif (type == 'versioning')
        status = Metadata.first({:conditions => ['sites_id = ? and key = ?', siteid, app_config[:COMPLIANCE_VERSIONING_ACTIVATION_CONFIG]]})
        if (!status.nil?)
          return status.value
        else
          return nil
        end
      else
        return nil
      end
    end

    private_class_method :getFeatureStatus
  end

end