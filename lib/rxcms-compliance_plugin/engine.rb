require 'rubygems'
require 'flag_shih_tzu'
require 'paper_trail'

module RxcmsCompliancePlugin
  extend ::ActiveSupport::Autoload

  autoload :PaperTrail
  autoload :FlagShihTzu

  class Engine < ::Rails::Engine

    config.generators do |g|
      g.test_framework :rspec
      # g.fixture_replacement :factory_girl, :dir => 'spec/factories'
    end

    config.autoload_paths += Dir["#{config.root}/lib/rxcms-compliance_plugin/classes/**/"]

  end
end
