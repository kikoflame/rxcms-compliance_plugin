$(function(){
    // Automatically perform installation
    $("#installationBtn").on("click", function(e){
        e.stopPropagation();

        $("#installationStatus").text("Installation process is under way, please wait...");
        $.post("/compliance/installer/before_process", function(response){
            if (response.status == "success")
            {
                location.reload();
            } else
            {
                $("#installationStatus").text("Installation failed, please contact extension developer for more information!");
            }
        }).error(function(){
                $("#installationStatus").text("Installation failed because the server is down. Please try again later");
            });
    });
});