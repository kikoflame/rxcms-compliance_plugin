function getComplianceFeatureStatus()
{
    $.get("/compliance/services/feature/status", function(response){
        if (response.status == "success")
        {
            if (response.data == "yes")
            {
                $("input[name=complianceFeature][value=on]").prop("checked",true);
            } else if (response.data == "no")
            {
                $("input[name=complianceFeature][value=off]").prop("checked",true);
            }
        } else
        {
            Messenger().post("unable to get compliance feature status");
        }
    }).error(function(){
        Messenger().post("unable to contact server");
    });
}

function getVersioningFeatureStatus()
{
    $.get("/compliance/services/feature/versioning/status", function(response){
        if (response.status == "success")
        {
            if (response.data == "yes")
            {
                $("input[name=versioningFeature][value=on]").prop("checked",true);
                $("input[name=complianceFeature]").removeAttr("disabled")
            } else if (response.data == "no")
            {
                $("input[name=versioningFeature][value=off]").prop("checked",true);

                $("input[name=complianceFeature]").attr("disabled","disabled");
                $("input[name=complianceFeature][value=off]").prop("checked",true);
            }
        } else
        {
            Messenger().post("unable to get versioning feature status");
        }
    }).error(function(){
            Messenger().post("unable to contact server");
        });
}

$(function(){

    Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
        theme: 'air'
    };

    // Automatically load compliance feature status on page load
    getComplianceFeatureStatus();
    getVersioningFeatureStatus();

    // Track value of input[name=complianceFeature]
    $("input[name=complianceFeature]").on("change", function(e){
        e.stopPropagation();

        var value = $(this).val() == "on" ? "yes" : "no";
        var versioningValue = $("input[name=versioningFeature]:checked").val() == "on" ? "yes" : "no"

        if (versioningValue == "no")
        {
            Messenger().post("versioning is required, please activate it");
            return;
        }

        $("input[name=complianceFeature]").attr("disabled","disabled");
        $.post("/compliance/services/feature/status/set",
        {
            "value" : value
        }
        ,function(response){
            if (response.status == "success")
            {
                $("input[name=complianceFeature]").removeAttr("disabled");
                Messenger().post("compliance status is set successfully");
            } else
            {
                $("input[name=complianceFeature]").removeAttr("disabled");
                Messenger().post("unable to set compliance feature status");
            }
        }).error(function(){
            $("input[name=complianceFeature]").removeAttr("disabled");
            Messenger().post("unable to contact server");
        });
    });

    $("input[name=versioningFeature]").on("change", function(e){
        e.stopPropagation();

        var value = $(this).val() == "on" ? "yes" : "no";

        if (value == "no")
        {
            $("input[name=complianceFeature][value=off]").prop("checked",true)
            $("input[name=complianceFeature]").attr("disabled","disabled");
        }
        else if (value == "yes")
        {
            $("input[name=complianceFeature]").removeAttr("disabled");
        }

        $("input[name=versioningFeature]").attr("disabled","disabled");
        $.post("/compliance/services/feature/versioning/status/set",
        {
            "value" : value
        }, function(response){
            if (response.status == "success")
            {
                $("input[name=versioningFeature]").removeAttr("disabled");
                Messenger().post("versioning status is set successfully");
            } else
            {
                $("input[name=versioningFeature]").removeAttr("disabled");
                Messenger().post("unable to set versioning feature status");
            }
        }).error(function(){
            $("input[name=versioningFeature]").removeAttr("disabled");
            Messenger().post("unable to contact server");
        });
    });

    // Uninstall
    $("#uninstallBtn").on("click", function(){
        var $this = $(this);
        $this.attr("disabled","disabled");
        $.post("/compliance/installer/uninstall", function(response){
            if (response.status == "success")
            {
                Messenger().post("Successfully uninstalled, redirecting...");
                location.reload();
            }
            else
            {
                $this.removeAttr("disabled");
                Messenger().post("Unable to uninstall component");
            }
        }).error(function(){
                Messenger().post("Unable to contact server");
                $this.removeAttr("disabled");
            })
    });
});
