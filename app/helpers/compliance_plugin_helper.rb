module CompliancePluginHelper

  def current_user
    if UserSession.find.nil?
      @current_user ||= nil
    else
      @current_user ||= User.find(UserSession.find.record.id)
    end
  end

  # Get current user's role
  def get_current_user_role
    @current_user = current_user
    if (!@current_user.nil?)
      siteId = session[:accessible_appid]
      roleId = session[:accessible_roleid]

      if (!siteId.nil? && !roleId.nil?)
        userRole = Role.find(roleId)
        @curUserRole = userRole.name
      else
        @curUserRole = 'loggedin'
      end

    else
      @curUserRole = 'anonymous'
    end
  end

end