class ComplianceInstallerController < ApplicationController
  include CompliancePluginHelper

  layout false

  before_filter :get_current_user_role

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/compliance/compliance_config.yml', __FILE__))))

  # Each step should return JSON status "success", "failure" or "unimplemented"

  # Used for initializing and creating database entries
  def before_process
    SymmetricEncryption.load!

    begin
      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      if !Metadata.first({:conditions => ['key = ? and sites_id = ?', APP_CONFIG[:COMPLIANCE_ACTIVATION_CONFIG], session[:accessible_appid]]}).nil?
        Metadata.first({:conditions => ['key = ? and sites_id = ?', APP_CONFIG[:COMPLIANCE_ACTIVATION_CONFIG], session[:accessible_appid]]}).destroy
      end
      if !Metadata.first({:conditions => ['key = ? and sites_id = ?', APP_CONFIG[:COMPLIANCE_VERSIONING_ACTIVATION_CONFIG], session[:accessible_appid]]}).nil?
        Metadata.first({:conditions => ['key = ? and sites_id = ?', APP_CONFIG[:COMPLIANCE_VERSIONING_ACTIVATION_CONFIG], session[:accessible_appid]]}).destroy
      end

      Metadata.create!({
                           :key => APP_CONFIG[:COMPLIANCE_ACTIVATION_CONFIG],
                           :value => 'no',
                           :cat => APP_CONFIG[:COMPLIANCE_CAT_CONFIG],
                           :mime => "text/plain",
                           :sites_id => session[:accessible_appid]
                       })
      Metadata.create!({
                           :key => APP_CONFIG[:COMPLIANCE_VERSIONING_ACTIVATION_CONFIG],
                           :value => 'no',
                           :cat => APP_CONFIG[:COMPLIANCE_CAT_CONFIG],
                           :mime => "text/plain",
                           :sites_id => session[:accessible_appid]
                       })

      render :json => {:status => 'success'}
    rescue Exception => ex
      render :json => {:status => 'failure', :message => ex.message}
    end
  end

  # Used for logical processing
  def core_process
    render :json => {:status => 'unimplemented'}
  end

  # Used for configuring data
  def post_process
    render :json => {:status => 'unimplemented'}
  end

  # Uninstaller
  def uninstall
    begin
      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      # Delete all configuration items
      if !Metadata.first({:conditions => ['key = ? and sites_id = ?', APP_CONFIG[:COMPLIANCE_ACTIVATION_CONFIG], session[:accessible_appid]]}).nil?
        Metadata.first({:conditions => ['key = ? and sites_id = ?', APP_CONFIG[:COMPLIANCE_ACTIVATION_CONFIG], session[:accessible_appid]]}).destroy
      end
      if !Metadata.first({:conditions => ['key = ? and sites_id = ?', APP_CONFIG[:COMPLIANCE_VERSIONING_ACTIVATION_CONFIG], session[:accessible_appid]]}).nil?
        Metadata.first({:conditions => ['key = ? and sites_id = ?', APP_CONFIG[:COMPLIANCE_VERSIONING_ACTIVATION_CONFIG], session[:accessible_appid]]}).destroy
      end

      render :json => {:status => 'success'}
    rescue Exception => ex
      render :json => {:status => 'failure', :message => ex.message}
    end
  end

  private

end
