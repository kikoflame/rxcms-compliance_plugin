class ComplianceEngineController < ApplicationController
  include CompliancePluginHelper

  layout false

  before_filter :get_current_user_role, :except => [
      :index
  ]

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/compliance/compliance_config.yml', __FILE__))))

  # Allow all to access index
  # Disallow all to access configure and installer, except for admin

  # Write your readme here
  def index

  end

  def configure
    # Check access
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'loggedin' ||
        @curUserRole == 'anonymous')
      raise 'unauthorized access'
    end

    config = Metadata.first({ :conditions => ['sites_id = ? and cat = ? and key = ?', session[:accessible_appid], APP_CONFIG[:COMPLIANCE_CAT_CONFIG], APP_CONFIG[:COMPLIANCE_ACTIVATION_CONFIG]]})

    if (!defined?(PaperTrail))
      raise 'paper_trail gem is required for this gem to work'
    end

    if (config.nil?)
      raise 'configuration item for compliance plugin is not present'
    end

    if request.xhr?
      respond_to do |t|
        t.html
      end
    else
      raise 'unauthorized access'
    end
  end

  def installer
    # Check access
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'loggedin' ||
        @curUserRole == 'anonymous')
      raise 'unauthorized access'
    end

    if request.xhr?
      respond_to do |t|
        t.html
      end
    else
      raise 'unauthorized access'
    end
  end

  private

end
