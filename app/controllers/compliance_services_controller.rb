class ComplianceServicesController < ApplicationController
  include CompliancePluginHelper

  layout false

  before_filter :get_current_user_role

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/compliance/compliance_config.yml', __FILE__))))

  # Get compliance feature status
  def get_compliance_feature_status
    begin

      status = Metadata.first({ :conditions => ['sites_id = ? and key = ?', session[:accessible_appid], APP_CONFIG[:COMPLIANCE_ACTIVATION_CONFIG]] })
      if (!status.nil?)
        render :json => { :status => 'success', :data => status.value }
      else
        raise "status doesn't exist"
      end

    rescue Exception => ex
      render :json => { :status => 'failure', :message => ex.message }
    end
  end

  def get_versioning_status
    begin

      status = Metadata.first({ :conditions => ['sites_id = ? and key = ?', session[:accessible_appid], APP_CONFIG[:COMPLIANCE_VERSIONING_ACTIVATION_CONFIG]] })
      if (!status.nil?)
        render :json => { :status => 'success', :data => status.value }
      else
        raise "status doesn't exist"
      end

    rescue Exception => ex
      render :json => { :status => 'failure', :message => ex.message }
    end
  end

  # Set compliance feature status
  def set_compliance_feature_status
    begin

      status = Metadata.first({ :conditions => ['sites_id = ? and key = ?', session[:accessible_appid], APP_CONFIG[:COMPLIANCE_ACTIVATION_CONFIG]] })
      if (!status.nil?)
        Metadata.update(status.id, { :value => params[:value] })
        render :json => { :status => 'success' }
      else
        raise "compliance status doesn't exist"
      end

    rescue Exception => ex
      render :json => { :status => 'failure', :message => ex.message }
    end
  end

  def set_versioning_status
    begin

      status = Metadata.first({ :conditions => ['sites_id = ? and key = ?', session[:accessible_appid], APP_CONFIG[:COMPLIANCE_VERSIONING_ACTIVATION_CONFIG]] })
      if (!status.nil?)
        if (params[:value] == 'no')
          Metadata.paper_trail_off
          complianceStatus = Metadata.first({ :conditions => ['sites_id = ? and key = ?', session[:accessible_appid], APP_CONFIG[:COMPLIANCE_ACTIVATION_CONFIG]] })
          if (!complianceStatus.nil?)
            Metadata.update(complianceStatus.id, { :value => 'no' })
          end
        else
          Metadata.paper_trail_on
        end
        Metadata.update(status.id, { :value => params[:value] })
        render :json => { :status => 'success' }
      else
        raise "versioning status doesn't exist"
      end

    rescue Exception => ex
      render :json => { :status => 'failure', :message => ex.message }
    end
  end

  private

end
