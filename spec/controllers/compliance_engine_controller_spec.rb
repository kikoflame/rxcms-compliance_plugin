require 'spec_helper'

describe ComplianceEngineController do

  before do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())
    TUserSession = Struct.new(:record)
    TUserSessionAttrs = Struct.new(:id)
    TUser = Struct.new(:login,:email)

    UserSession.stub(:find).and_return TUserSession.new(TUserSessionAttrs.new(1))
    User.stub(:find).and_return TUser.new('admin','test@test.com')
  end

  after do

  end

  describe "GET #index" do
    before do

    end

    after do

    end

    it "should return index page" do
      get :index

      response.should be_success
    end
  end

  describe "GET #configure" do
    before do
      stub_const("Role", double())
      TUserRole = Struct.new(:name)
    end

    after do

    end

    it "should return configure page" do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('admin')
      Metadata.should_receive(:first).and_return true

      get :configure

      response.body.should_not be_empty
    end

    it "should not return configure page if user is not logged in" do
      User.stub(:find).and_return nil

      expect { get :configure }.to raise_error
    end

    it "should not return configure page if user is just logged in" do
      session[:accessible_appid] = nil
      session[:accessible_roleid] = nil

      expect { get :configure }.to raise_error
    end

    it "should not return configure page if request type is not xhr" do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      Role.should_receive(:find).and_return TUserRole.new('admin')
      controller.request.should_receive(:xhr?).at_least(:once).and_return false

      expect { get :configure }.to raise_error
    end
  end

  describe "GET #installer" do
    before do
      stub_const("Role", double())
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1
      TUserRole = Struct.new(:name)
    end

    after do

    end

    it "should return installer page" do
      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('admin')

      get :installer

      response.body.should_not be_empty
    end

    it "should not return installer page if user is not logged in" do
      Role.should_receive(:find).and_return TUserRole.new('anonymous')

      expect { get :installer }.to raise_error
    end
  end

end
