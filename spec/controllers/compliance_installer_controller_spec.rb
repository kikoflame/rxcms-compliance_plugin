require 'spec_helper'

describe ComplianceInstallerController do
  before do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())
    stub_const("SymmetricEncryption", double())
    stub_const("Role", double())
    TUserSession = Struct.new(:record)
    TUserSessionAttrs = Struct.new(:id)
    TUser = Struct.new(:login,:email)
    TUserRole = Struct.new(:name)

    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1
    Role.should_receive(:find).and_return TUserRole.new('admin')

    UserSession.stub(:find).and_return TUserSession.new(TUserSessionAttrs.new(1))
    User.stub(:find).and_return TUser.new('admin','test@test.com')
  end

  after do

  end

  describe "POST #before_process" do
    before do
      TMetadata = Struct.new(:id, :destroy)
      Metadata.stub(:first) { TMetadata.new(1, true) }
    end

    after do

    end

    it "should before process" do
      SymmetricEncryption.should_receive(:load!).and_return true
      Metadata.stub(:destroy).and_return true
      Metadata.should_receive(:create!).at_least(:once).and_return true

      post :before_process

      response.should be_success
      parsed = JSON.parse(response.body)
      parsed['status'].should == "success"
    end
  end

  describe "POST #core_process" do
    before do

    end

    after do

    end

    it "should core process" do

      post :core_process

      response.should be_success
      parsed = JSON.parse(response.body)
      parsed['status'].should == "unimplemented"
    end
  end

  describe "POST #post_process" do
    before do

    end

    after do

    end

    it "should post process" do

      post :post_process

      response.should be_success
      parsed = JSON.parse(response.body)
      parsed['status'].should == "unimplemented"
    end
  end

  describe "POST #uninstall" do
    before do
      TMetadata = Struct.new(:id, :destroy)
      Metadata.stub(:first) { TMetadata.new(1, true) }
    end

    after do

    end

    it "should uninstall" do

      post :uninstall

      response.should be_success
      parsed = JSON.parse(response.body)
      parsed['status'].should == "success"
    end
  end

end
