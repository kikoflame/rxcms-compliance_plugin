require 'spec_helper'

describe ComplianceServicesController do
  before do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())
    TMetadata = Struct.new(:id, :value)

    TUserSession = Struct.new(:record)
    TUserSessionAttrs = Struct.new(:id)
    TUser = Struct.new(:login,:email)

    UserSession.stub(:find).and_return TUserSession.new(TUserSessionAttrs.new(1))
    User.stub(:find).and_return TUser.new('admin','test@test.com')
  end

  after do

  end

  it "should get compliance feature status" do
    Metadata.should_receive(:first).and_return TMetadata.new(1, "on")

    get :get_compliance_feature_status

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should get versioning status" do
    Metadata.should_receive(:first).and_return TMetadata.new(1, "on")

    get :get_versioning_status

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should set compliance feature status" do

    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new(1, "no")
    Metadata.should_receive(:update).at_least(:once).and_return true

    post :set_compliance_feature_status, { :value => 'yes' }

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should set versioning status" do

    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new(1, "no")
    Metadata.should_receive(:update).at_least(:once).and_return true
    Metadata.should_receive(:paper_trail_on).at_least(:once).and_return true

    post :set_versioning_status, { :value => 'yes' }

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end

  it "should set versioning status when params = 0" do

    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new(1, "no")
    Metadata.should_receive(:update).at_least(:once).and_return true
    Metadata.should_receive(:paper_trail_off).at_least(:once).and_return true

    post :set_versioning_status, { :value => 'no' }

    response.should be_success
    parsed = JSON.parse(response.body)
    parsed['status'].should == 'success'
  end
end
