require 'spec_helper'

describe RxcmsCompliancePlugin::Status do
  before do
    stub_const("Metadata", double())
  end

  after do

  end

  it "should get statuses" do
    Metadata.stub(:first) { Metadata }
    Metadata.should_receive(:value).at_least(:once).and_return []

    RxcmsCompliancePlugin::Status.isEnabled?(1).should_not be_empty
  end

end