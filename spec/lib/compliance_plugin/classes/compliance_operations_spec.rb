require 'spec_helper'

describe RxcmsCompliancePlugin::ComplianceOperations do
  before do
    stub_const("Metadata", double())
    stub_const("Version", double())
    stub_const("Release", double())
    TMetadata = Struct.new(:value)
    TCatMetadata = Struct.new(:cat)
    TRelease = Struct.new(:users_id, :destroy)
  end

  after do

  end

  it "should process content" do
    TObject = Struct.new(:id, :archived?, :version_at)
    TInstanceRelease = Struct.new(:id, :date_time)
    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new("yes")
    Release.should_receive(:find_by_metadata_id).and_return TInstanceRelease.new(1, nil)

    expect { RxcmsCompliancePlugin::ComplianceOperations.process_content(1, TObject.new(1, false, true)) }.to_not raise_error
  end

  it "should process content with flag" do
    TObject = Struct.new(:id, :archived?, :version_at)
    TInstanceRelease = Struct.new(:id, :date_time)
    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new("yes")
    Release.should_receive(:find_by_metadata_id).and_return TInstanceRelease.new(1, nil)

    expect { RxcmsCompliancePlugin::ComplianceOperations.process_content_with_flag(1, TObject.new(1, false, true)) }.to_not raise_error
  end

  it "should update but not publish if users and publishers are the same" do
    data = { :name => "dummy", :value => "dummy" }
    whodunnit = { :whodunnit => "1" }
    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new("yes")
    Metadata.stub(:update).and_return true
    Version.stub(:select) { Version }
    Version.stub(:where) { Version }
    Version.stub(:first).and_return whodunnit

    expect { RxcmsCompliancePlugin::ComplianceOperations.update(1,data,1,["cat"],1,DateTime.now.to_s) }.to raise_error
  end

  it "should update and not publish if users and publishers are not the same" do
    data = { :name => "dummy", :value => "dummy" }
    whodunnit = { :whodunnit => "1" }
    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new("yes")
    Metadata.should_receive(:find).at_least(:once).and_return TCatMetadata.new("cat")
    Metadata.should_receive(:update).and_return true
    Version.stub(:select) { Version }
    Version.stub(:where) { Version }
    Version.should_receive(:first).and_return whodunnit
    Release.should_receive(:first).and_return nil
    Release.should_receive(:create).and_return true
    Metadata.should_receive(:paper_trail_on).at_least(:once).and_return true

    expect { RxcmsCompliancePlugin::ComplianceOperations.update(1,data,2,["cat"], 1,DateTime.now.to_s) }.to_not raise_error
  end

  it "should update when the content is current" do
    data = { :name => "dummy", :value => "dummy" }
    whodunnit = { :whodunnit => "1" }
    Metadata.should_receive(:update).and_return true
    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new("yes")
    Metadata.should_receive(:find).at_least(:once).and_return TCatMetadata.new("cat")
    Release.should_receive(:first).and_return TRelease.new(1, true)
    Release.stub(:destroy).and_return true
    Release.should_receive(:create).and_return true
    Metadata.should_receive(:paper_trail_on).at_least(:once).and_return true

    expect { RxcmsCompliancePlugin::ComplianceOperations.update(1,data,2,["cat"],1,"current") }.to_not raise_error
  end

  it "should destroy permanently" do
    TMetadataItem = Struct.new(:id,:destroy)

    Metadata.should_receive(:first).and_return TMetadataItem.new(1, true)
    Metadata.stub(:destroy).and_return true
    Release.should_receive(:first).and_return TRelease.new(1, true)
    Version.should_receive(:destroy_all).and_return true
    Metadata.should_receive(:paper_trail_off).at_least(:once).and_return true

    expect { RxcmsCompliancePlugin::ComplianceOperations.destroy_permanently(1) }.to_not raise_error
  end

  it "should be able to restore" do
    TMetadataArchived = Struct.new(:id,:archived,:save)

    Metadata.should_receive(:find).and_return TMetadataArchived.new(1, true, true)
    Metadata.stub(:save).and_return true

    expect { RxcmsCompliancePlugin::ComplianceOperations.restore(1) }.to_not raise_error
  end

  it "should be able to destroy when versioning is on" do
    TAnotherMetadata = Struct.new(:destroy, :cat, :archived, :save)

    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new("yes")

    expect { RxcmsCompliancePlugin::ComplianceOperations.destroy(1, ['cat'], TAnotherMetadata.new(true, 'cat', false)) }.to_not raise_error
  end

  it "should be able to destroy when versioning is off" do
    TAnotherMetadata = Struct.new(:destroy, :cat, :archived, :save)

    Metadata.should_receive(:first).at_least(:once).and_return TMetadata.new("no")
    Metadata.should_receive(:paper_trail_off).and_return true

    expect { RxcmsCompliancePlugin::ComplianceOperations.destroy(1, ['cat'], TAnotherMetadata.new(true, 'cat', false)) }.to_not raise_error
  end
end